package net.cysquatch.bamboo.plugins.attach;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

public class ConfluenceAttacher
{
    private String url;
    private String user;
    private String pass;
    private String space;
    private String fileName;
    private String pageTitle;
    private String contentType;
    private String comment;
    private String errorText;
    private long fileSize;

    ConfluenceAttacher(String confluenceURL, String userName, String password, String spaceKey, String pageTitle,
                       String fileName, String contentType, String comment)
    {
        url = confluenceURL;
        user = userName;
        pass = password;
        space = spaceKey;
        this.pageTitle = pageTitle;
        this.fileName = fileName;
        this.contentType = contentType;
        this.comment = comment;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public String getErrorText()
    {
        return errorText;
    }

    private static byte[] toByteArray(File file) throws FileNotFoundException, IOException {
        int length = (int) file.length();
        byte[] array = new byte[length];
        InputStream in = new FileInputStream(file);
        int offset = 0;
        while (offset < length) {
            int count = in.read(array, offset, (length - offset));
            offset += length;
        }
        in.close();
        return array;
    }


    public boolean doAttach()
    {
        try
        {
            XmlRpcClient rpcClient;
            XmlRpcClientConfigImpl config;

            config = new XmlRpcClientConfigImpl();
            config.setServerURL(new URL(url));
            rpcClient = new XmlRpcClient();
            rpcClient.setConfig(config);


            // Login and retrieve logon token
            Vector loginParams = new Vector(2);
            loginParams.add(user);
            loginParams.add(pass);
            String loginToken = (String) rpcClient.execute("confluence1.login", loginParams);

            // Get the page as page id is required to make attachment
            Vector getPageTokenVector = new Vector(3);
            getPageTokenVector.add(loginToken);
            getPageTokenVector.add(space);
            getPageTokenVector.add(pageTitle);
            Map pageMap = (Map) rpcClient.execute("confluence1.getPage", getPageTokenVector);

            File file = new File(fileName);

            Map attachment = new Hashtable();
            attachment.put("fileName", file.getName());
            attachment.put("contentType", contentType);
            attachment.put("comment", comment);

            Vector attachVect = new Vector(4);
            attachVect.add(loginToken);
            attachVect.add(pageMap.get("id"));
            attachVect.add(attachment);

            long length = file.length();
            byte[]  content = toByteArray(file);

            attachVect.add(content);
            rpcClient.execute("confluence1.addAttachment", attachVect);

            // Log out
            Vector logoutTokenVector = new Vector(1);
            logoutTokenVector.add(loginToken);
            Boolean bool = (Boolean) rpcClient.execute("confluence1.logout", logoutTokenVector);
        }
        catch (java.lang.Throwable e)
        {
            StringWriter writer = new StringWriter();
            e.printStackTrace(new PrintWriter(writer));
            errorText = writer.toString();
            return false;
        }
        return true;
    }

}
