package net.cysquatch.bamboo.plugins.attach;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

public class ConnectionValidator
{
    private String errorText;

    public boolean validate(String url, String user, String pass)
    {
            XmlRpcClient rpcClient;
            XmlRpcClientConfigImpl config;

            try {
                config = new XmlRpcClientConfigImpl();
                config.setServerURL(new URL(url));
                config.setConnectionTimeout(8000);
                rpcClient = new XmlRpcClient();
                rpcClient.setConfig(config);
            }
            catch(MalformedURLException e) {
                errorText = e.getMessage();
                return false;
            }

            // Login and retrieve logon token
            Vector loginParams = new Vector(2);
            loginParams.add(user);
            loginParams.add(pass);
            try {
                String loginToken = (String) rpcClient.execute("confluence1.login", loginParams);
                Vector logoutTokenVector = new Vector(1);
                logoutTokenVector.add(loginToken);
                rpcClient.execute("confluence1.logout", logoutTokenVector);
            }
            catch(org.apache.xmlrpc.XmlRpcException e)
            {
                errorText = e.getMessage();
                return false;
            }
            return true;
    }

    public String getErrorText()
    {
        return errorText;
    }
}


