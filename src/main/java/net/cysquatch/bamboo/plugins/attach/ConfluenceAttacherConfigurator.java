package net.cysquatch.bamboo.plugins.attach;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.opensymphony.xwork.TextProvider;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

public class ConfluenceAttacherConfigurator extends AbstractTaskConfigurator
{
    private TextProvider textProvider;

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put("url", params.getString("url"));
        config.put("confluence-user", params.getString("confluence-user"));
        config.put("confluence-pass", params.getString("confluence-pass"));
        config.put("filename", params.getString("filename"));
        config.put("space", params.getString("space"));
        config.put("pagetitle", params.getString("pagetitle"));
        config.put("contenttype", params.getString("contenttype"));
        config.put("comment", params.getString("comment"));
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);

        //context.put("say", "Hello, World!");
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);

        context.put("url", taskDefinition.getConfiguration().get("url"));
        context.put("confluence-user", taskDefinition.getConfiguration().get("confluence-user"));
        context.put("confluence-pass", taskDefinition.getConfiguration().get("confluence-pass"));
        context.put("filename", taskDefinition.getConfiguration().get("filename"));
        context.put("space", taskDefinition.getConfiguration().get("space"));
        context.put("pagetitle", taskDefinition.getConfiguration().get("pagetitle"));
        context.put("contenttype", taskDefinition.getConfiguration().get("contenttype"));
        context.put("comment", taskDefinition.getConfiguration().get("comment"));
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForView(context, taskDefinition);

        context.put("url", taskDefinition.getConfiguration().get("url"));
        context.put("confluence-user", taskDefinition.getConfiguration().get("confluence-user"));
        context.put("confluence-pass", taskDefinition.getConfiguration().get("confluence-pass"));
        context.put("filename", taskDefinition.getConfiguration().get("filename"));
        context.put("space", taskDefinition.getConfiguration().get("space"));
        context.put("pagetitle", taskDefinition.getConfiguration().get("pagetitle"));
        context.put("contenttype", taskDefinition.getConfiguration().get("contenttype"));
        context.put("comment", taskDefinition.getConfiguration().get("comment"));
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);

        String url = params.getString("url");
        String user = params.getString("confluence-user");
        String pass = params.getString("confluence-pass");

        ConnectionValidator validator = new ConnectionValidator();
        boolean success = validator.validate(url, user, pass);
        if(!success)
        {
            errorCollection.addError("url", validator.getErrorText());
        }

    }

    public void setTextProvider(final TextProvider textProvider)
    {
        this.textProvider = textProvider;
    }
}
