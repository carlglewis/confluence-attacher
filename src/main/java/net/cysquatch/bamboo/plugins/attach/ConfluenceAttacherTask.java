package net.cysquatch.bamboo.plugins.attach;

import java.util.Collection;
import java.util.Iterator;
import java.io.File;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.plan.artifact.ArtifactContext;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinitionContext;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.types.PatternSet;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;

public class ConfluenceAttacherTask implements TaskType
{
    @NotNull
    @java.lang.Override
    public TaskResult execute(@NotNull final TaskContext taskContext) throws TaskException
    {
        final BuildLogger buildLogger = taskContext.getBuildLogger();

        final String url         = taskContext.getConfigurationMap().get("url");
        final String user        = taskContext.getConfigurationMap().get("confluence-user");
        final String pass        = taskContext.getConfigurationMap().get("confluence-pass");
        final String filename    = taskContext.getConfigurationMap().get("filename");
        final String space       = taskContext.getConfigurationMap().get("space");
        final String pagetitle   = taskContext.getConfigurationMap().get("pagetitle");
        final String contenttype = taskContext.getConfigurationMap().get("contenttype");
        final String comment     = taskContext.getConfigurationMap().get("comment");

        String[] files = getFilesForCopyPattern(taskContext, filename);
        if(files.length  == 0)
        {
            String msg = "No files found for pattern: " + filename + ", nothing will be attached";
            return TaskResultBuilder.create(taskContext).success().build();
        }

        String configMsg = "Uploading files to " + url + " as user: " + user;
        buildLogger.addBuildLogEntry(configMsg);
        configMsg = "Attaching files to page: " + pagetitle + " in space: " + space;
        buildLogger.addBuildLogEntry(configMsg);

        ConfluenceAttacher attacher = new ConfluenceAttacher(url, user, pass, space,
            pagetitle, "", contenttype, comment);

        for (String file : files)
        {
            String fullFileName = taskContext.getRootDirectory() + File.separator + file;
            attacher.setFileName(fullFileName);
            boolean result = attacher.doAttach();
            if(!result) {
                buildLogger.addBuildLogEntry(attacher.getErrorText());
                return TaskResultBuilder.create(taskContext).failed().build();
            }
            File fileObj = new File(fullFileName);
            String size = Long.toString(fileObj.length());
            String msg = "Attached file: " + fullFileName + ", " + size + " bytes";
            buildLogger.addBuildLogEntry(msg);
        }

        return TaskResultBuilder.create(taskContext).success().build();
    }

    void printArtifacts(@NotNull final TaskContext taskContext)
    {
        final BuildLogger buildLogger = taskContext.getBuildLogger();

       BuildContext bc = taskContext.getBuildContext();
       ArtifactContext ac = bc.getArtifactContext();
       java.util.Collection<ArtifactDefinitionContext> adcList = ac.getDefinitionContexts();

       Iterator<ArtifactDefinitionContext> iter = adcList.iterator();
       while(iter.hasNext())
       {
            ArtifactDefinitionContext ctx = iter.next();
            // String info = ctx.getId() + " " + ctx.getName() + " " + ctx.getCopyPattern() + " " + ctx.getLocation();
            // buildLogger.addBuildLogEntry(info);
            logArtifactFilesFound(taskContext, ctx); 
       }        
    }

    private String[] getFilesForCopyPattern(@NotNull final TaskContext taskContext, String pattern)
    {
        FileSet fileSet = new FileSet();
        final PatternSet.NameEntry include = fileSet.createInclude();
        include.setName(pattern);
        fileSet.setDir(taskContext.getRootDirectory());
        fileSet.setProject(new Project());

        final DirectoryScanner scanner = fileSet.getDirectoryScanner();
        scanner.scan();
        return scanner.getIncludedFiles();
    }

    private String[] getFilesForArtifact(@NotNull final TaskContext taskContext, ArtifactDefinitionContext artifact)
    {
        File artifactLocation = taskContext.getRootDirectory();
        if (StringUtils.isNotEmpty(artifact.getLocation()))
        {
            artifactLocation = new File(artifactLocation, artifact.getLocation());
        }

        FileSet fileSet = new FileSet();
        final PatternSet.NameEntry include = fileSet.createInclude();
        include.setName(artifact.getCopyPattern());
        fileSet.setDir(artifactLocation);
        fileSet.setProject(new Project());

        final DirectoryScanner scanner = fileSet.getDirectoryScanner();
        scanner.scan();
        return scanner.getIncludedFiles();
    }

    public void logArtifactFilesFound(@NotNull final TaskContext taskContext, ArtifactDefinitionContext artifact)             
    {
        String [] fileArray = getFilesForArtifact(taskContext, artifact);

        final BuildLogger log = taskContext.getBuildLogger();
        for (final String fileName : fileArray)
        {
            String msg = "For artifact " + artifact.getName() + " found file at: " + fileName;
            log.addBuildLogEntry(msg);
        }
    }
}
